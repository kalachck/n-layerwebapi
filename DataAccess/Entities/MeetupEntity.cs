﻿using DataAccess.Entities.Abstract;

namespace DataAccess.Entities
{
    public class MeetupEntity : BaseEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Speaker { get; set; }

        public DateTime Date { get; set; }

        public List<UserEntity> Users { get; set; }
    }
}
