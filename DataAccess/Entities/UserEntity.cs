﻿using DataAccess.Entities.Abstract;

namespace DataAccess.Entities
{
    public class UserEntity : BaseEntity
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public string RefreshToken { get; set; }

        public DateTime RefreshTokenExpiryTime { get; set; }

        public int MeetupId { get; set; }

        public MeetupEntity Meetup { get; set; }
    }
}
