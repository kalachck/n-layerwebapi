﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class nlayerMigration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RefreshToken",
                table: "Users",
                type: "varchar(MAX)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Meetups",
                keyColumn: "Id",
                keyValue: 1,
                column: "Meeting Date",
                value: new DateTime(2022, 10, 23, 14, 23, 27, 831, DateTimeKind.Utc).AddTicks(9946));

            migrationBuilder.UpdateData(
                table: "Meetups",
                keyColumn: "Id",
                keyValue: 2,
                column: "Meeting Date",
                value: new DateTime(2022, 10, 23, 14, 23, 27, 831, DateTimeKind.Utc).AddTicks(9949));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RefreshToken",
                table: "Users",
                type: "varchar",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(MAX)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Meetups",
                keyColumn: "Id",
                keyValue: 1,
                column: "Meeting Date",
                value: new DateTime(2022, 10, 23, 10, 32, 45, 120, DateTimeKind.Utc).AddTicks(9598));

            migrationBuilder.UpdateData(
                table: "Meetups",
                keyColumn: "Id",
                keyValue: 2,
                column: "Meeting Date",
                value: new DateTime(2022, 10, 23, 10, 32, 45, 120, DateTimeKind.Utc).AddTicks(9600));
        }
    }
}
