﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class nlayerMigration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Meetups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    Speaker = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MeetingDate = table.Column<DateTime>(name: "Meeting Date", type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meetups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Password = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false),
                    RefreshToken = table.Column<string>(type: "varchar", nullable: true),
                    RefreshTokenExpiryTime = table.Column<DateTime>(type: "date", nullable: false),
                    MeetupId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Meetups_MeetupId",
                        column: x => x.MeetupId,
                        principalTable: "Meetups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Meetups",
                columns: new[] { "Id", "Meeting Date", "Description", "Speaker", "Title" },
                values: new object[] { 1, new DateTime(2022, 10, 23, 10, 32, 45, 120, DateTimeKind.Utc).AddTicks(9598), "someMeetup", "Noris", "title" });

            migrationBuilder.InsertData(
                table: "Meetups",
                columns: new[] { "Id", "Meeting Date", "Description", "Speaker", "Title" },
                values: new object[] { 2, new DateTime(2022, 10, 23, 10, 32, 45, 120, DateTimeKind.Utc).AddTicks(9600), null, null, "title2" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Login", "MeetupId", "Password", "RefreshToken", "RefreshTokenExpiryTime" },
                values: new object[] { 1, "admin", 1, "admin", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Login", "MeetupId", "Password", "RefreshToken", "RefreshTokenExpiryTime" },
                values: new object[] { 2, "user", 1, "user", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.CreateIndex(
                name: "IX_Users_MeetupId",
                table: "Users",
                column: "MeetupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Meetups");
        }
    }
}
