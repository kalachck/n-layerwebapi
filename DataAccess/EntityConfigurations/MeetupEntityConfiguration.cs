﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityConfigurations
{
    public class MeetupEntityConfiguration : IEntityTypeConfiguration<MeetupEntity>
    { 
        public void Configure(EntityTypeBuilder<MeetupEntity> builder)
        {
            builder.ToTable("Meetups");

            builder.HasKey(m => m.Id);

            builder.Property(m => m.Id)
                .ValueGeneratedOnAdd();

            builder.Property(m => m.Title)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnType("varchar");

            builder.Property(m => m.Description)
                .IsRequired(false)
                .HasMaxLength(200)
                .HasColumnType("varchar");

            builder.Property(m => m.Speaker)
                .IsRequired(false)
                .HasMaxLength(30)
                .HasColumnType("varchar");

            builder.Property(m => m.Date)
                .IsRequired()
                .HasColumnType("date")
                .HasColumnName("Meeting Date");

            builder.HasData(
                new MeetupEntity { Id = 1, Title = "title", Description = "someMeetup", Speaker = "Noris", Date = System.DateTime.UtcNow },
                new MeetupEntity { Id = 2, Title = "title2", Date = System.DateTime.UtcNow });
        }
    }
}
