﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityConfigurations
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");

            builder.HasOne(u => u.Meetup)
                .WithMany(m => m.Users)
                .HasForeignKey(u => u.MeetupId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasKey(u => u.Id);

            builder.Property(m => m.Id)
                .ValueGeneratedOnAdd();

            builder.Property(u => u.Login)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnType("varchar");

            builder.Property(u => u.Password)
                .IsRequired()
                .HasMaxLength(36)
                .HasColumnType("varchar");

            builder.Property(u => u.RefreshToken)
                .IsRequired(false)
                .HasColumnType("varchar(MAX)");

            builder.Property(u => u.RefreshTokenExpiryTime)
                .HasColumnType("date");

            builder.HasData(
                new UserEntity { Id = 1, Login = "admin", MeetupId = 1, Password = "admin" },
                new UserEntity { Id = 2, Login = "user", MeetupId = 1, Password = "user" });
        }
    }
}
