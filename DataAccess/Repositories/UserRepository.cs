﻿using DataAccess.Entities;
using DataAccess.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext _applicationContext;

        public UserRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<UserEntity> GetAsync(string login, string password)
        {
            return await _applicationContext.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Login == login && u.Password == password);
        }

        public async Task<UserEntity> GetByRefreshTokenAsync(string refreshToken)
        {
            return await _applicationContext.Users.AsNoTracking().FirstOrDefaultAsync(u => u.RefreshToken == refreshToken);
        }

        public async Task UpsertAsync(UserEntity userEntity)
        {
            if (!await _applicationContext.Users.ContainsAsync(userEntity))
            {
                _applicationContext.Entry(userEntity).State = EntityState.Added;
            }
            else
            {
                _applicationContext.Entry(userEntity).State = EntityState.Modified;
            }

            await _applicationContext.SaveChangesAsync();
        }
    }
}
