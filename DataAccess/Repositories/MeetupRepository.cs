﻿using DataAccess.Entities;
using DataAccess.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class MeetupRepository : IMeetupRepository
    {
        private readonly ApplicationContext _applicationContext;

        public MeetupRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<List<MeetupEntity>> TakeAsync(int amount)
        {
            return await _applicationContext.Meetups.AsNoTracking().Take(amount).ToListAsync();
        }

        public async Task<MeetupEntity> GetAsync(int id)
        {
            return await _applicationContext.Meetups.AsNoTracking().FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task UpsertAsync(MeetupEntity meetupEntity)
        {
            if (_applicationContext.Meetups.Any(entity => entity.Id == meetupEntity.Id))
            {
                _applicationContext.Entry(meetupEntity).State = EntityState.Modified;
            }
            else
            {
                _applicationContext.Entry(meetupEntity).State = EntityState.Added;
            }

            await _applicationContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(MeetupEntity meetupEntity)
        {
            _applicationContext.Entry(meetupEntity).State = EntityState.Deleted;
            await _applicationContext.SaveChangesAsync();
        }
    }
}
