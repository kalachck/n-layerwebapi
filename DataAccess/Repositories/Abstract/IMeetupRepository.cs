﻿using DataAccess.Entities;

namespace DataAccess.Repositories.Abstract
{
    public interface IMeetupRepository
    {
        Task<List<MeetupEntity>> TakeAsync(int amount);

        Task<MeetupEntity> GetAsync(int id);

        Task UpsertAsync(MeetupEntity meetupEntity);

        Task DeleteAsync(MeetupEntity meetupEntity);
    }
}
