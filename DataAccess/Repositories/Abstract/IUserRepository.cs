﻿using DataAccess.Entities;

namespace DataAccess.Repositories.Abstract
{
    public interface IUserRepository
    {
        Task<UserEntity> GetAsync(string login, string password);

        Task<UserEntity> GetByRefreshTokenAsync(string refreshToken);

        Task UpsertAsync(UserEntity userEntity);
    }
}
