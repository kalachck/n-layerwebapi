﻿using System.Security.Claims;

namespace Domain.Generators.Abstract
{
    public interface ITokenGenerator
    {
        Task<string> GenerateAccesTokenAsync(IEnumerable<Claim> claims);

        Task<string> GenerateRefreshTokenAsync();
    }
}
