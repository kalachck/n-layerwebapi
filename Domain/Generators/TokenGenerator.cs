﻿using Domain.Generators.Abstract;
using Domain.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Domain.Generators
{
    public class TokenGenerator : ITokenGenerator
    {
        private readonly TokenOptions _tokenOptions;

        public TokenGenerator(TokenOptions tokenOptions)
        {
            _tokenOptions = tokenOptions;
        }

        public async Task<string> GenerateAccesTokenAsync(IEnumerable<Claim> claims)
        {
            var token = new JwtSecurityToken
                (
                    issuer: _tokenOptions.Issuer,
                    audience: _tokenOptions.Audience,
                    notBefore: DateTime.UtcNow,
                    claims: claims,
                    expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(int.Parse(_tokenOptions.Lifetime))),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenOptions.Key)), SecurityAlgorithms.HmacSha256)
                );

            return await Task.FromResult(new JwtSecurityTokenHandler().WriteToken(token));
        }

        public async Task<string> GenerateRefreshTokenAsync()
        {
            var randomNumber = new byte[32];

            using (var rng = System.Security.Cryptography.RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
            }

            return await Task.FromResult(Convert.ToBase64String(randomNumber));
        }
    }
}
