﻿using DataAccess.Entities;
using DataAccess.Repositories.Abstract;
using Domain.Generators.Abstract;
using Domain.Providers.Abstract;
using Domain.Results;
using System.Security.Claims;

namespace Domain.Providers
{
    public class AuthorizationProvider : IAuthorizationProvider
    {
        private readonly ITokenGenerator _tokenGenerator;

        private readonly IUserRepository _userRepository;

        private readonly IMeetupRepository _meetupRepository;

        public AuthorizationProvider(ITokenGenerator tokenGenerator, IUserRepository userRepository, IMeetupRepository meetupRepository)
        {
            _tokenGenerator = tokenGenerator;

            _userRepository = userRepository;

            _meetupRepository = meetupRepository;
        }

        public async Task<Result<string>> LoginAsync(string login, string password)
        {
            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password))
            {
                var user = await _userRepository.GetAsync(login, password);

                if (user != null)
                {
                    var claims = new List<Claim> { new Claim("login", user.Login) };

                    var accessToken = await _tokenGenerator.GenerateAccesTokenAsync(claims);

                    if (string.IsNullOrEmpty(user.RefreshToken))
                    {
                        user.RefreshToken = await _tokenGenerator.GenerateRefreshTokenAsync();
                        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(30);

                        await _userRepository.UpsertAsync(user);
                    }

                    return await Task.FromResult(new Result<string>() { Value = accessToken });
                }
            }

            return await Task.FromResult(new Result<string>() { Exception = "Invalid login or password" });
        }

        public async Task<Result<string>> RegisterAsync(string login, string password, int? meetupId)
        {
            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password) &&
                meetupId != null && await _meetupRepository.GetAsync(meetupId.Value) != null)
            {
                await _userRepository.UpsertAsync(new UserEntity() { Login = login, Password = password, MeetupId = meetupId.Value });

                return await Task.FromResult(new Result<string>() { Value = "Registration passed successfully" });
            }

            return await Task.FromResult(new Result<string>() { Exception = "Invalid data, please try again" });
        }

        public async Task<Result<string>> RefreshAsync(string refreshToken)
        {
            if (refreshToken != null)
            {
                var user = await _userRepository.GetByRefreshTokenAsync(refreshToken);

                if (user != null && user.RefreshTokenExpiryTime <= DateTime.UtcNow)
                {
                    var claims = new List<Claim> { new Claim("login", user.Login) };

                    var accessToken = await _tokenGenerator.GenerateAccesTokenAsync(claims);

                    user.RefreshToken = await _tokenGenerator.GenerateRefreshTokenAsync();
                    user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(30);

                    await _userRepository.UpsertAsync(user);

                    return await Task.FromResult(new Result<string>() { Value = accessToken });
                }
            }

            return await Task.FromResult(new Result<string>() { Exception = "Invalid token" });
        }
    }
}
