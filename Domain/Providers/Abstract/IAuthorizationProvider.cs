﻿using Domain.Results;

namespace Domain.Providers.Abstract
{
    public interface IAuthorizationProvider
    {
        Task<Result<string>> LoginAsync(string login, string password);

        Task<Result<string>> RegisterAsync(string login, string password, int? meetupId);

        Task<Result<string>> RefreshAsync(string refreshToken);
    }
}
