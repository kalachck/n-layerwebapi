﻿namespace Domain.DTO
{
    public class Meetup
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Speaker { get; set; }

        public DateTime Date { get; set; }

        public List<User> Users { get; set; }
    }
}
