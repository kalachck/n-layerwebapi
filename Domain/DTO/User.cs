﻿namespace Domain.DTO
{
    public class User
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string RefreshToken { get; set; }

        public DateTime RefreshTokenExpiryTime { get; set; }

        public int MeetupId { get; set; }

        public Meetup Meetup { get; set; }
    }
}
