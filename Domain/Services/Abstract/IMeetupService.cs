﻿using Domain.DTO;

namespace Domain.Services.Abstract
{
    public interface IMeetupService
    {
        Task<List<Meetup>> TakeAsync(int amount);

        Task<Meetup> GetAsync(int id);

        Task CreateAsync(Meetup meetup);

        Task UpdateAsync(Meetup meetup);

        Task DeleteAsync(int id);
    }
}
