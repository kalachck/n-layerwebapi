﻿using AutoMapper;
using DataAccess.Entities;
using DataAccess.Repositories.Abstract;
using Domain.DTO;
using Domain.Services.Abstract;

namespace Domain.Services
{
    public class MeetupService : IMeetupService
    {
        private readonly IMeetupRepository _meetupRepository;

        private readonly IMapper _mapper;

        public MeetupService(IMeetupRepository meetupRepository, IMapper mapper)
        {
            _meetupRepository = meetupRepository;

            _mapper = mapper;
        }

        public async Task<List<Meetup>> TakeAsync(int amount)
        {
            var meetups = await _meetupRepository.TakeAsync(amount);

            return _mapper.Map<List<Meetup>>(meetups);
        }

        public async Task<Meetup> GetAsync(int id)
        {
            var meetup = await _meetupRepository.GetAsync(id);

            return _mapper.Map<Meetup>(meetup);
        }

        public async Task CreateAsync(Meetup meetup)
        {
            await _meetupRepository.UpsertAsync(_mapper.Map<MeetupEntity>(meetup));
        }

        public async Task UpdateAsync(Meetup meetup)
        {
            await _meetupRepository.UpsertAsync(_mapper.Map<MeetupEntity>(meetup));
        }

        public async Task DeleteAsync(int id)
        {
            var meetup = await _meetupRepository.GetAsync(id);

            await _meetupRepository.DeleteAsync(_mapper.Map<MeetupEntity>(meetup));
        }
    }
}
