﻿namespace Domain.Options
{
    public class TokenOptions
    {
        public const string Token = "Token";

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string Lifetime { get; set; }

        public string Key { get; set; }
    }
}
