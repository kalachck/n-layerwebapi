﻿namespace Domain.Results
{
    public class Result<T>
    {
        public T Value { get; set; }

        public string Exception { get; set; }

        public Result()
        { }

        public bool HasValue()
        {
            return Exception == null;
        }

        public static implicit operator bool(Result<T> result)
        {
            return result.HasValue();
        }
    }
}
