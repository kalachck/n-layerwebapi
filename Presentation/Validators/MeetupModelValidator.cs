﻿using FluentValidation;
using Presentation.Models;

namespace Presentation.Validators
{
    public class MeetupModelValidator : AbstractValidator<MeetupModel>
    {
        public MeetupModelValidator()
        {
            RuleFor(meetup => meetup.Title).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(meetup => meetup.Speaker).MaximumLength(30);
            RuleFor(meetup => meetup.Date).NotEmpty().NotNull();
        }
    }
}
