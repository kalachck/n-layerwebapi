﻿using Domain.Providers.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Route("api")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IAuthorizationProvider _authorizationProvider;

        public UserController(IAuthorizationProvider authorizationProvider)
        {
            _authorizationProvider = authorizationProvider;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<Token>> LoginAsync(string login, string password)
        {
            var result = await _authorizationProvider.LoginAsync(login, password);

            if (result)
            {
                return Ok(new Token()
                {
                    AccessToken = result.Value
                });
            }

            return BadRequest(result.Exception);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<ActionResult> RegisterAsync(string login, string password, int? meetupId)
        {
            var result = await _authorizationProvider.RegisterAsync(login, password, meetupId.Value);

            if (result)
            {
                return Ok(result.Value);
            }

            return BadRequest(result.Exception);
        }
    }
}
