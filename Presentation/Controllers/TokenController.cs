﻿using Domain.Providers.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Authorize]
    [Route("api/token")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IAuthorizationProvider _authorizationProvider;

        public TokenController(IAuthorizationProvider authorizationProvider)
        {
            _authorizationProvider = authorizationProvider;
        }

        [HttpPost]
        [Route("refresh")]
        public async Task<ActionResult> Refresh(string refreshToken)
        {
            var result = await _authorizationProvider.RefreshAsync(refreshToken);

            if (result)
            {
                return Ok(new Token()
                {
                    AccessToken = result.Value
                });
            }

            return BadRequest(result.Exception);
        }
    }
}
