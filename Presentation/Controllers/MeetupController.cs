﻿using AutoMapper;
using Domain.DTO;
using Domain.Services.Abstract;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MeetupController : ControllerBase
    {
        private readonly IMeetupService _meetupService;

        private readonly IMapper _mapper;

        private readonly IValidator<MeetupModel> _validator;

        public MeetupController(IMeetupService meetupService, IMapper mapper, IValidator<MeetupModel> validator)
        {
            _meetupService = meetupService;

            _mapper = mapper;

            _validator = validator;
        }

        [HttpGet("take/{amount}")]
        public async Task<ActionResult<List<MeetupModel>>> TakeAsync(int amount)
        {
            var meetups = await _meetupService.TakeAsync(amount);

            if (meetups != null)
            {
                return _mapper.Map<List<MeetupModel>>(meetups);
            }

            return NotFound(404);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MeetupModel>> GetAsync(int id)
        {
            var meetupModel = await _meetupService.GetAsync(id);

            if (meetupModel != null)
            {
                return _mapper.Map<MeetupModel>(meetupModel);
            }

            return NotFound(404);
        }

        [HttpPost]
        public async Task<ActionResult> PostAsync(MeetupModel meetupModel)
        {
            if (meetupModel != null && _validator.Validate(meetupModel).IsValid)
            {
                await _meetupService.CreateAsync(_mapper.Map<Meetup>(meetupModel));

                return Ok();
            }

            return BadRequest("Invalid data! Please try again.");
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutAsync(int id, MeetupModel meetupModel)
        {
            if (meetupModel != null && _validator.Validate(meetupModel).IsValid && await _meetupService.GetAsync(id) != null)
            {
                var meetup = _mapper.Map<Meetup>(meetupModel);

                meetup.Id = id;

                await _meetupService.UpdateAsync(meetup);

                return Ok();
            }

            return BadRequest("Invalid data! Please try again.");
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (await _meetupService.GetAsync(id) != null)
            {
                await _meetupService.DeleteAsync(id);

                return NoContent();
            }

            return BadRequest("Invalid data! Please try again.");
        }
    }
}
