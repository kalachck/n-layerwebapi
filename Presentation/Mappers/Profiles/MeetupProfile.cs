﻿using AutoMapper;
using DataAccess.Entities;
using Domain.DTO;
using Presentation.Models;

namespace Presentation.Mappers.Profiles
{
    public class MeetupProfile : Profile
    {
        public MeetupProfile()
        {
            CreateMap<MeetupModel, Meetup>().ReverseMap();
            CreateMap<Meetup, MeetupEntity>().ReverseMap();
        }
    }
}
