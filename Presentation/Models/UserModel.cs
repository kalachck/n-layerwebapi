﻿namespace Presentation.Models
{
    public class UserModel
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public int MeetupId { get; set; }

        public MeetupModel Meetup { get; set; }
    }
}
