﻿using System;
using System.Collections.Generic;

namespace Presentation.Models
{
    public class MeetupModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Speaker { get; set; }

        public DateTime Date { get; set; }

        public List<UserModel> Users { get; set; }
    }
}
