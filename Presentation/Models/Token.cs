﻿namespace Presentation.Models
{
    public class Token
    {
        public string AccessToken { get; set; }
    }
}
